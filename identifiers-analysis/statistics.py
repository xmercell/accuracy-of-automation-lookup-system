from openpyxl import load_workbook
import matplotlib.pyplot as plt
import numpy as np

#To run this just change identifier and file_name
identifier = "" #possible options - DOI, ZBL, MR
file_name = "" #possible files - zbl.xlsx, mr.xlsx, doi.xlsx, mr_advanced.xlsx
not_found = "NO-MATCHING" if identifier != "DOI" else "NON-MATCHING"
column_verified = "E" if identifier != "DOI" else "G"

def found_mr(found, verified):
    try:
        return int(found[2:]) == int(verified[2:])
    except:
        return False

def found_equal_to_verified(found, verified):
    if identifier == "MR":
        return found == verified or found_mr(found,verified)
    return found == verified 
def found_is_different_from_verified(found, verified):
    if identifier == "MR":
        return found != verified \
                and not found_is_empty_and_verified_is_empty(found, verified) \
                and not found_is_empty_and_verified_is_not_empty(found, verified) \
                and not found_equal_to_verified(found, verified)
    return found != verified \
            and not found_is_empty_and_verified_is_empty(found, verified) \
            and not found_is_empty_and_verified_is_not_empty(found, verified) 
def found_is_empty_and_verified_is_empty(found, verified):
    return found is None and verified == not_found
def found_is_empty_and_verified_is_not_empty(found, verified):
    return found is None and verified != not_found

def parse_table():
    variable_found_equal_to_verified = 0
    variable_found_is_different_from_verified = 0 
    variable_found_is_empty_and_verified_is_empty = 0
    variable_found_is_empty_and_verified_is_not_empty = 0
    wb = load_workbook(filename = file_name)
    sheet_ranges = wb['Sheet1']
    for i in range(2,202):
        found = sheet_ranges[f"D{i}"].value
        verified = sheet_ranges[f"{column_verified}{i}"].value

        if found_equal_to_verified(found, verified):
            variable_found_equal_to_verified += 1
        if found_is_different_from_verified(found, verified):
            variable_found_is_different_from_verified += 1
        if found_is_empty_and_verified_is_empty(found, verified):
            variable_found_is_empty_and_verified_is_empty += 1
        if found_is_empty_and_verified_is_not_empty(found, verified):
            variable_found_is_empty_and_verified_is_not_empty += 1

    plot_graph(variable_found_equal_to_verified,variable_found_is_different_from_verified,variable_found_is_empty_and_verified_is_empty,variable_found_is_empty_and_verified_is_not_empty)


def func(pct, allvals):
    absolute = int(np.round(pct/100.*np.sum(allvals)))
    return "{:.1f}%".format(pct, absolute)

def plot_graph(variable_found_equal_to_verified,variable_found_is_different_from_verified,variable_found_is_empty_and_verified_is_empty,variable_found_is_empty_and_verified_is_not_empty):
    
    labels = 'Úspešne nájdené', 'Úspešne nenájdené','Neúspešne nájdené', 'Neúspešne nenájdené'
    fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))

    recipe = [f"ÚSPECH: {identifier} existuje a nástroj reportuje {identifier} správneho záznamu",
            f"ÚSPECH: {identifier} neexistuje a nástroj {identifier} nereportuje",
            f"NEÚSPECH: Nástroj reportuje {identifier} iného záznamu",
            f"NEÚSPECH: {identifier} existuje, ale nástroj {identifier} nereportuje"]

    colours = {'Úspešne nájdené': '#3aeb34',
            'Úspešne nenájdené': '#a4eb34',
           'Neúspešne nájdené': '#eb3434',
           'Neúspešne nenájdené': '#eb5934'}

    data = [variable_found_equal_to_verified, variable_found_is_empty_and_verified_is_empty,variable_found_is_different_from_verified, variable_found_is_empty_and_verified_is_not_empty]

    wedges, texts, autotexts = ax.pie(data,autopct=lambda pct: func(pct, data),
                                  textprops=dict(color="w"),colors=[colours[key] for key in labels],wedgeprops = {"edgecolor" : "black",
                      'linewidth': 1,
                      'antialiased': True})

    plt.setp(autotexts, size=11, weight="bold")
    plt.legend(wedges,recipe, bbox_to_anchor=(1,0.5), loc="center right", fontsize=10, 
          bbox_transform=plt.gcf().transFigure)
    plt.subplots_adjust(left=0.0, bottom=0.1, right=0.45)
    plt.show()

if __name__ == '__main__':
    parse_table()
