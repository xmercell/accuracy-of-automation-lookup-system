from openpyxl import load_workbook
import matplotlib.pyplot as plt
import numpy as np
import math 

def excel_without_text_equal_to_attribute_analysis():    
    wb = load_workbook(filename = 'doi_analysis.xlsx')
    sheet_ranges = wb['Sheet1']
    lst_positive = []
    lst_negative = []
    for i in range(2,502):
        doi_text = sheet_ranges[f"B{i}"].value
        doi_text_score = float(sheet_ranges[f"C{i}"].value) if sheet_ranges[f"C{i}"].value is not None else None 
        doi_attributes = sheet_ranges[f"D{i}"].value
        doi_attributes_score = float(sheet_ranges[f"E{i}"].value) if sheet_ranges[f"E{i}"].value is not None else None
        doi_verified = sheet_ranges[f"F{i}"].value

        #Skip those, where doi_text is equal to doi_attributes
        if doi_text is not None and doi_attributes is not None and doi_text == doi_attributes:
            continue

        #Adding to lst positive results
        if doi_text is not None and doi_attributes is not None and (doi_text == doi_verified or doi_attributes == doi_verified):
            lst_positive.append(max(doi_text_score,doi_attributes_score))
        if doi_text is None and doi_attributes is not None and doi_attributes == doi_verified:
            lst_positive.append(doi_attributes_score)
        if doi_attributes is None and doi_text is not None and doi_text == doi_verified:
            lst_positive.append(doi_text_score)
        
        #Adding to lst1 negative results
        if doi_text is not None and doi_attributes is not None and (doi_text != doi_verified and doi_attributes != doi_verified):
            lst_negative.append(max(doi_text_score,doi_attributes_score))
        if doi_text is None and doi_attributes is not None and doi_attributes != doi_verified:
            lst_negative.append(doi_attributes_score)
        if doi_attributes is None and doi_text is not None and doi_text != doi_verified:
            lst_negative.append(doi_text_score)

    return lst_positive, lst_negative

def excel_attribute_equal_to_text_analysis():
    wb = load_workbook(filename = 'doi_analysis.xlsx')
    sheet_ranges = wb['Sheet1']
    lst_positive = []
    lst_negative = []
    for i in range(2,502):
        doi_text = sheet_ranges[f"B{i}"].value
        doi_text_score = float(sheet_ranges[f"C{i}"].value) if sheet_ranges[f"C{i}"].value is not None else None 
        doi_attributes = sheet_ranges[f"D{i}"].value
        doi_attributes_score = float(sheet_ranges[f"E{i}"].value) if sheet_ranges[f"E{i}"].value is not None else None
        doi_verified = sheet_ranges[f"F{i}"].value
        
        #Adding positive results where doi_text is equal to doi_attributes and both of them are equal to doi_verified
        if doi_text is not None and doi_attributes is not None and doi_text == doi_attributes and doi_text == doi_verified:
            lst_positive.append(max(doi_text_score,doi_attributes_score))
        
        #Adding negative results where doi_text is equal to doi_attributes and both of them are not equal to doi_verified
        if doi_text is not None and doi_attributes is not None and doi_text == doi_attributes and doi_text != doi_verified:
            lst_negative.append(max(doi_text_score,doi_attributes_score))

    return lst_positive, lst_negative

def find_optimal_breakpoint(positive, negative):
    weight_positive, weight_negative = 1, 4
    low, high =math.floor(min(positive + negative)), math.ceil(max(positive + negative))
    values = {}
    breakpoint = low
    while breakpoint <= high:
        value = weight_positive * len([v for v in positive if v < breakpoint]) + weight_negative * len([v for v in negative if v > breakpoint])
        values[breakpoint] = value
        breakpoint += 0.1
    return min(values, key=values.get)

if __name__ == '__main__':
    lst_positive_equal, lst_negative_equal = excel_attribute_equal_to_text_analysis()
    lst_positive, lst_negative = excel_without_text_equal_to_attribute_analysis()
    optimal_breakpoint_equal = find_optimal_breakpoint(lst_positive_equal,lst_negative_equal)
    optimal_breakpoint_not_equal = find_optimal_breakpoint(lst_positive,lst_negative)
    print(f"Optimal breakpoint for DOI where doi_text is equal to doi_attribute is {optimal_breakpoint_equal:.2f}")
    print(f"Optimal breakpoint for DOI where doi_text is empty or doi_attribute is empty, or doi_text is not equal to doi_attribute {optimal_breakpoint_not_equal:.2f}")

